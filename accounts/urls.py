from django.urls import path
from accounts.views import user_login, user_Logout, user_signup

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_Logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
